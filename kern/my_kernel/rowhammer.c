/**
 *
 * Rowhammer.c - this is a kernel module that, when inserted,
 * will vmalloc a lot of data and rowhammer it.
 * loop_count is self-explanatory, should be varied.
 * Need to work on malloc data appropriately, and selecting
 * which rows to try to hammer.
 * 
 *
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/sched.h>	/* For current */
#include <linux/tty.h>		/* For the tty declarations */
#include <linux/version.h>	/* For LINUX_VERSION_CODE */
#include <linux/slab.h>


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Marshall Versteeg");


static void print_string(char *str)
{
	struct tty_struct *my_tty;
	#if ( LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,5) )
		my_tty = current->tty;
	#else
		my_tty = current->signal->tty;
	#endif

		if (my_tty != NULL) {
			((my_tty->ops)->write) (my_tty,	/* The tty itself */
	#if ( LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,9) )		
						   0,	/* Don't take the string 
							   from user space        */
	#endif
						   str,	/* String                 */
						   strlen(str));	/* Length */
	#if ( LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,9) )		
			((my_tty->ops)->write) (my_tty, 0, "\015\012", 2);
	#else
			((my_tty->ops)->write) (my_tty, "\015\012", 2);
	#endif
	}
}


static int __init print_string_init(void)
{
	int * rowhammer_mem;
	int row_size = 4096; //not sure about this....
	int num_rows = 7;
	int loop_count = 50000000;
	int const_val = 0xAAAAAAAA;
	int i;
	int * addr1;
	int * addr2;
	int err = 0;
	int * fakeaddr1;
	int * fakeaddr2;

	//Allocate 5 contiguous DRAM rows
	rowhammer_mem = (int *) kmalloc(row_size * num_rows, GFP_USER);
	if(!rowhammer_mem){
		print_string("Failed to kmalloc, exiting...");
		return 0;
	}
	i = 0;	
	for(; i < row_size * num_rows / sizeof(int); ++i){
		rowhammer_mem[i] = const_val;
	}
	print_string("Initialized mem, checking for errors before hammer");

	i = 0;	
	for(; i < row_size * num_rows / sizeof(int); ++i){
		if(rowhammer_mem[i] != const_val){
			++err;
			printk(KERN_ALERT "Error at mem[%d]: %d", (unsigned int) i, rowhammer_mem[i]);			
		}
	}

	if(err == 0)
		print_string("No Errors!");
	else
		print_string("'Errors' before hammer");



	// NOTES
	// Looks like we're getting 50000000 loops in ~14s ==>
	//			~114,000 accesses per 32ms
	// Varied row_size from 4096 ... 64 * 4096, all got no errors
	//
	// Got /proc/pid/pagemap of `insmod rowhammer.ko` in pmap.out!

	//TODO
	//Implement kernel module init multithreaded... (Me)
	//See if we can change refresh rate by updating Rpi firmware (Brad)
	// Check that rowbuffer is being flushed (timing)
	// Check physical address to DRAM row mapping


	//Hammer two rows adjacent to middle
	//Hopefully it doesn't hammer other stuff nearby our mem
	addr1 = rowhammer_mem +  ((row_size * num_rows) / 2 - 1);
	addr2 = rowhammer_mem +  ((row_size * num_rows) / 2 + 1);
	// addr2 = addr1 + sizeof(int);
	print_string("Starting hammer....");
	for(i = 0; i < loop_count; ++i){
	 	asm volatile(
	 		"ldr r2, [%0];"
	 		"dsb ish;"
	 		"ldr r2, [%1];"
	 		"dsb ish;"
	 		"mcr p15, 0, %0, c7, c14, 1;"  // DCCIMVAC: Data cache clean and invalidate by MVA to PoC
	 		"mcr p15, 0, %1, c7, c14, 1;"  //These two lines clflush addresses r4, r5
	 		// "mcr p15, 0, r6, c7, c14, 1;" //Using these instead prove that cache lines are being
	 		// "mcr p15, 0, r7, c7, c14, 1;" //flushed, b/c it runs a lot faster with these lines isntesad
	 		"dsb ish;"
	 		:
	 		: "r" (addr1),
	 		  "r" (addr2)
	 	);

	}
 	
	// fakeaddr1 = rowhammer_mem;
	// fakeaddr2 = rowhammer_mem + sizeof(int);
 // 	asm volatile
	//     (
	//     // "mov r3, %0;"                   // r3 = #loop_count
	//     // "mov r4, %1;"                   // r4 = addr1
	//     // "mov r5, %2;"                   // r5 = addr2
	//     // "mov r6, %3;"
	//     // "mov r7, %4;"
	//     // "mrc p15, 0, %0, c9, c13, 0;"   // t1 = rdtsc() -- I would remove this
	//     // "dsb ish;"
	// "ds_loop:;"
	//     "ldr r2, [%0];"
	//     "dsb ish;"
	//     "ldr r2, [%1];"
	//     "dsb ish;"
	//     "mcr p15, 0, %0, c7, c14, 1;"  // DCCIMVAC: Data cache clean and invalidate by MVA to PoC
	//     "mcr p15, 0, %1, c7, c14, 1;"  //These two lines clflush addresses r4, r5
	//     // "mcr p15, 0, r6, c7, c14, 1;" //Using these instead prove that cache lines are being
	//     // "mcr p15, 0, r7, c7, c14, 1;" //flushed, b/c it runs a lot faster with these lines isntesad
	//     "dsb ish;"
	//     "subs r3, r3, #1;"
	//     "bne ds_loop;"
	//     "dsb ish;"
	//     // "mrc p15, 0, %1, c9, c13, 0;"   // t2 = rdtsc() -- Remove this too
 //            : 
 //            : "r" (loop_count),
 //              "r" (addr1),
 //              "r" (addr2),
 //              "r" (fakeaddr1),
 //              "r" (fakeaddr2)
 //            : "r2", "r3", "r4", "r5", "memory"
	// );

    print_string("Finished hammering, checking for errors...");

	i = 0;	
	for(; i < row_size * num_rows / sizeof(int); ++i){
		if(rowhammer_mem[i] != const_val){
			++err;
			printk(KERN_ALERT "Error at mem[%d]: %d", (unsigned int) i, rowhammer_mem[i]);			
		}
	}

	if(err == 0)
		print_string("No Errors :(");
	else
		print_string("Errors!");

	printk(KERN_ALERT "Total # errors: %d", err);


    kfree(rowhammer_mem);


	return 0;
}

static void __exit print_string_exit(void)
{

	print_string("The module has been removed.  Farewell world!");
}

module_init(print_string_init);
module_exit(print_string_exit);



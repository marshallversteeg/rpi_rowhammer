/**

	TODO:
	- Have one thread rowhammer addr1 and garbageaddr
	    and another thread hammer addr2 and garbageaddr
	    to avoid hitting on the rowbuffer between threads
	- 

 */


#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/kthread.h>  // for threads
#include <linux/sched.h>  // for task_struct
#include <linux/time.h>   // for using jiffies 
#include <linux/jiffies.h>
#include <linux/timer.h>
#include <linux/tty.h>		/* For the tty declarations */
#include <linux/version.h>	/* For LINUX_VERSION_CODE */
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/smp.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Marshall Versteeg");

extern unsigned long volatile jiffies;

static struct task_struct *thread1;
static struct task_struct *thread2;

// static const int MEM_SIZE = 4*8*131072; //This is the max
static const int MEM_SIZE = 32768; //for testing
// static const int NUM_SECONDS = 300; //total time we would like

static void print_string(char *);

static int thread_fn_v2(void * void_pntr) {
	// int LOOP_COUNT = NUM_SECONDS * 2900000 / (MEM_SIZE / 4);
	int LOOP_COUNT = 1000000; //1000k
	int i = 0;
	int ** params;
	int * addr1;
	int * addr2;
	params = (int **) void_pntr;
	addr1 = params[0];
	addr2 = params[1];

	//What the fuck, commenting this out breaks the whole thing.
	printk(KERN_INFO "addr1: %p  addr2: %p", addr1, addr2);
     for(i = 0; i < LOOP_COUNT; ++i){
		asm volatile(
			// "mov r0, r0;"
			// "mov r1, r1;"
			// "mov r2, r2;"
			// "mov r3, r3;"
			// "mov r0, r0;"
			// "mov r1, r1;"
 		"ldr r2, [%0];"
 		"dsb ish;"
 		"ldr r2, [%1];"
 		"dsb ish;"
 		"mcr p15, 0, %0, c7, c14, 1;"  // DCCIMVAC: Data cache clean and invalidate by MVA to PoC
 		"mcr p15, 0, %1, c7, c14, 1;"  //These two lines clflush addresses r4, r5
 		// // "mcr p15, 0, r6, c7, c14, 1;" //Using these instead prove that cache lines are being
 		// // "mcr p15, 0, r7, c7, c14, 1;" //flushed, b/c it runs a lot faster with these lines isntesad
 		"dsb ish;"
 		:
 		: "r" (addr1),
 		  "r" (addr2)
     	);
     }


	while(!kthread_should_stop());
	//kthread_should_stop, so we do that.
	do_exit(1);
	// return 0;
}

static int thread_test_init(void) {
	int * rowhammer_mem;
	int i = 0;
	int j = 0;
	int k = 0;
	int ret = 0;
	int const_val = 0xA55A5AA5;
	int err = 0;
	int pid;
	unsigned long prev_time, current_time;
	int ** params1;
	int ** params2;


	// printk(KERN_INFO "sizeof(int) == %d", sizeof(int)); //ITS 4
	prev_time = jiffies;
	pid = smp_processor_id();
	printk(KERN_INFO "master PID: %d", pid);
	printk(KERN_INFO "thread1 PID: %d", (pid + 1) % 4);
	printk(KERN_INFO "thread2 PID: %d", (pid + 2) % 4);
	params1 = (int **) kmalloc(sizeof(int *)*2, GFP_USER);
	params2 = (int **) kmalloc(sizeof(int *)*2, GFP_USER);

	/*=======================================
	=            Initialize MEM             =
	=======================================*/	
	rowhammer_mem = (int * ) kmalloc(MEM_SIZE, GFP_USER);
	printk(KERN_INFO "Size of kmalloc: %d", ksize(rowhammer_mem));

	if(!rowhammer_mem){
		print_string("Failed to kmalloc, exiting...");
		return 0;
	}
	i = 0;	
	for(; i < MEM_SIZE / sizeof(int); ++i){
		rowhammer_mem[i] = const_val;
	}
	printk(KERN_INFO "Rowhammer mem base virtual address: %p\n", rowhammer_mem);
	print_string("Initialized mem, checking for errors before hammer");

	/*=======================================
	=            Check for Errors           =
	=======================================*/	
	i = 0;	
	for(; i < MEM_SIZE / sizeof(int); ++i){
		if(rowhammer_mem[i] != const_val){
			++err;
			printk(KERN_ALERT "Error at mem[%d]: %d", (unsigned int) i, rowhammer_mem[i]);			
		}
	}
	if(err == 0)
		print_string("Okay, starting hammer...");
	else
		print_string("'Errors' before hammer");



	/*=======================================
	=            Start Rowhammer            =
	=======================================*/	

	params1[0]			= rowhammer_mem;
	params1[1]			= rowhammer_mem + (MEM_SIZE/(2*sizeof(int))) - sizeof(int);
	params2[0]			= rowhammer_mem;
	params2[1]			= rowhammer_mem + (MEM_SIZE/(2*sizeof(int))) - sizeof(int);
	printk(KERN_INFO "addrA:%p addrB:%p and garbage:%p",  params1[0], params2[0], params2[1]);

	// j = 0;
	// for(; j < MEM_SIZE / sizeof(int); ++j){
		i = 0;
		for(; i < MEM_SIZE / sizeof(int); i++){
			params2[0] = rowhammer_mem + (sizeof(int) * i);
			// thread1 = kthread_create(thread_fn_v2, (void *) params1, "thread1");
			thread2 = kthread_create(thread_fn_v2, (void *) params2, "thread2");
			// kthread_bind(thread1, ((pid + 1) % 4));
			kthread_bind(thread2, ((pid + 2) % 4));
		    
		    prev_time = jiffies;
		    
		    // if(thread1){
		    //     wake_up_process(thread1);
		    // }
		    if(thread2){
		        wake_up_process(thread2);
		    }
		    // msleep(1);
			//This is supposedly blocking, so we can just do this right away.
			// ret = kthread_stop(thread1);
			ret = kthread_stop(thread2);
			
			current_time = jiffies;
			printk(KERN_INFO "%lu jiffies for addrA:%p addrB:%p and garbage:%p", (current_time-prev_time), params1[0], params2[0], params2[1]);

			// print_string("Finished one interation");
			k = 0;	
			// rowhammer_mem[13] = rowhammer_mem[13] & ~0x0000F000; //yup, it shows errors
			for(; k < MEM_SIZE / sizeof(int); ++k){
				if(rowhammer_mem[k] != const_val){
					++err;
					print_string("Error detected!");
					printk(KERN_ALERT "Error at mem[%d]: %d", (unsigned int) k, rowhammer_mem[k]);			
					printk(KERN_ALERT "From addresses: %p and %p", params1[0], params2[0]);
				}
			}
		}
	// }



    print_string("Finished hammering, checking for errors...");

	if(err == 0)
		print_string("No Errors :(");
	else
		print_string("Errors!");
	printk(KERN_ALERT "Total # errors: %d", err);

    kfree(rowhammer_mem);
    kfree(params1);
    kfree(params2);
	return 0;
}



static void thread_test_exit(void) {
	printk(KERN_INFO "thread_test_exit\n\n");
}

module_init(thread_test_init);
module_exit(thread_test_exit);


static void print_string(char *str)
{
	struct tty_struct *my_tty;
	#if ( LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,5) )
		my_tty = current->tty;
	#else
		my_tty = current->signal->tty;
	#endif

		if (my_tty != NULL) {
			((my_tty->ops)->write) (my_tty,	/* The tty itself */
	#if ( LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,9) )		
						   0,	/* Don't take the string 
							   from user space        */
	#endif
						   str,	/* String                 */
						   strlen(str));	/* Length */
	#if ( LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,9) )		
			((my_tty->ops)->write) (my_tty, 0, "\015\012", 2);
	#else
			((my_tty->ops)->write) (my_tty, "\015\012", 2);
	#endif
	}
}

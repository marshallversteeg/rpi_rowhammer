
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/kthread.h>  // for threads
#include <linux/sched.h>  // for task_struct
#include <linux/time.h>   // for using jiffies 
#include <linux/timer.h>
#include <linux/tty.h>		/* For the tty declarations */
#include <linux/version.h>	/* For LINUX_VERSION_CODE */
#include <linux/slab.h>
#include <linux/delay.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Marshall Versteeg");

static struct task_struct *thread1;
static struct task_struct *thread2;
static struct task_struct *thread3;
static struct task_struct *thread4;

static const int ROW_SIZE = 4096;
static const int NUM_ROWS = 7;
static const int NUM_SECONDS = 30;

static void print_string(char *);

static int thread_fn(void * void_pntr) {
	int LOOP_COUNT = NUM_SECONDS * 100000;
	int i = 0;
	int * addr = (int *) void_pntr;
	
	printk(KERN_INFO "Thread awoken");

     for(i = 0; i < LOOP_COUNT; ++i){
		asm volatile(
     		"ldr r2, [%0];"
     		"dsb ish;"
     		// "ldr r2, [%1];"
     		// "dsb ish;"
     		"mcr p15, 0, %0, c7, c14, 1;"  // DCCIMVAC: Data cache clean and invalidate by MVA to PoC
     		// "mcr p15, 0, %1, c7, c14, 1;"  //These two lines clflush addresses r4, r5
     		// "mcr p15, 0, r6, c7, c14, 1;" //Using these instead prove that cache lines are being
     		// "mcr p15, 0, r7, c7, c14, 1;" //flushed, b/c it runs a lot faster with these lines isntesad
     		"dsb ish;"
     		:
     		: "r" (addr)/*,
     		  "r" (addr2)*/
     	);
     }
	while(!kthread_should_stop()){
	}
	do_exit(1);
	// return 0;
}

static int thread_test_init(void) {
	int * rowhammer_mem;
	int i = 0;
	int ret = 0;
	int const_val = 0xAAAAAAAA;
	int * addr1;
	int * addr2;
	int * addr3;
	int err = 0;

	rowhammer_mem = (int *) kmalloc(ROW_SIZE * NUM_ROWS, GFP_USER);
	if(!rowhammer_mem){
		print_string("Failed to kmalloc, exiting...");
		return 0;
	}
	i = 0;	
	for(; i < ROW_SIZE * NUM_ROWS / sizeof(int); ++i){
		rowhammer_mem[i] = const_val;
	}
	print_string("Initialized mem, checking for errors before hammer");

	i = 0;	
	for(; i < ROW_SIZE * NUM_ROWS / sizeof(int); ++i){
		if(rowhammer_mem[i] != const_val){
			++err;
			printk(KERN_ALERT "Error at mem[%d]: %d", (unsigned int) i, rowhammer_mem[i]);			
		}
	}

	if(err == 0)
		print_string("No Errors!");
	else
		print_string("'Errors' before hammer");

	addr1 = rowhammer_mem +  ((ROW_SIZE * NUM_ROWS) / 2/* - 1*/);
	addr2 = rowhammer_mem +  ((ROW_SIZE * NUM_ROWS) / 2 + 2);
	addr3 = rowhammer_mem +  ((ROW_SIZE * NUM_ROWS) / 2 + 2);

    printk(KERN_INFO "Creating threads...\n");
    thread1 = kthread_create(thread_fn, (void *) addr1, "thread1");
    thread2 = kthread_create(thread_fn, (void *) addr1, "thread2");
    thread3 = kthread_create(thread_fn, (void *) addr2, "thread3");
    thread4 = kthread_create(thread_fn, (void *) addr2, "thread4");
    kthread_bind(thread1, 0);
    kthread_bind(thread2, 1);
    kthread_bind(thread3, 2);
    kthread_bind(thread4, 3);
    if(thread1){
        wake_up_process(thread1);
    }
    if(thread2){
        wake_up_process(thread2);
    }
    if(thread3){
        wake_up_process(thread3);
    }
    if(thread4){
        wake_up_process(thread4);
    }


    print_string("Starting hammer....");
    for(i = 0; i < NUM_SECONDS; ++i){
    	msleep(1000);
    }

	ret = kthread_stop(thread1);
	ret = kthread_stop(thread2);
	ret = kthread_stop(thread3);
	ret = kthread_stop(thread4);

    print_string("Finished hammering, checking for errors...");

	i = 0;	
	for(; i < ROW_SIZE * NUM_ROWS / sizeof(int); ++i){
		if(rowhammer_mem[i] != const_val){
			++err;
			printk(KERN_ALERT "Error at mem[%d]: %d", (unsigned int) i, rowhammer_mem[i]);			
		}
	}

	if(err == 0)
		print_string("No Errors :(");
	else
		print_string("Errors!");

	printk(KERN_ALERT "Total # errors: %d", err);


    kfree(rowhammer_mem);


	return 0;
}



static void thread_test_exit(void) {
	printk(KERN_INFO "thread_test_exit\n\n");
}

module_init(thread_test_init);
module_exit(thread_test_exit);


static void print_string(char *str)
{
	struct tty_struct *my_tty;
	#if ( LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,5) )
		my_tty = current->tty;
	#else
		my_tty = current->signal->tty;
	#endif

		if (my_tty != NULL) {
			((my_tty->ops)->write) (my_tty,	/* The tty itself */
	#if ( LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,9) )		
						   0,	/* Don't take the string 
							   from user space        */
	#endif
						   str,	/* String                 */
						   strlen(str));	/* Length */
	#if ( LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,9) )		
			((my_tty->ops)->write) (my_tty, 0, "\015\012", 2);
	#else
			((my_tty->ops)->write) (my_tty, "\015\012", 2);
	#endif
	}
}

#!/bin/bash
sudo rmmod thread_test_v2
make
sleep 1
sudo dmesg -c
sudo insmod thread_test_v2.ko
rm captured_dmesg.out
while ps aux | grep "[t]hread_test_v2" > /dev/null ; do
	dmesg >> captured_dmesg.out
	sleep 60
done

echo "Run dmesg and look for most common jiffy #, then run"
echo "./check_dmesg.sh #"


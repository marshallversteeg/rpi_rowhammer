
//with two thread, double-ended hammer with two while loops (to hammer every combo)
//time = 19s for 2^15, 	5min for 2^17 O(mem_size^2)
//Can expect abuout 600 years for 2^30.
//But, we don't have to loop twice for thread since we'll map to all of mem

//double-ended hammer with one while loop
//Time = 48s for 2^17	1m34s for 2^18 O(mem_size)	=> 102 hours for 2^30

//double-ended hammer with one while loop and incrementing by rowsize (2048)
//Time = 25s for 2^18, 51s for 2^19 => 28 hours for 2^30

//Changed from 2 threads to 1 thread with 1 master, still no better timing.

//So that confirms they're still going to mem since we can see row buff hits
//Removed unnecessary "dsb ish" instructions, now seeing 19 - 27 jiffies
//and getting > 200k accesses / 32 ms refresh rate! So don't need multithreaded
//Changed HAMMER_LOOP_COUNT to 2mil => still covers 10 32ms refresh intervals
//
//Now 17s for 2^18, 33s for 2^19, 68s for 2^20==> 18 hours for 2^30

//Changing rowbuff_loop_count to 500k see jiffies as {9,10} or {13,14} set >=12 thresh
//	42s for 2^20, 81s for 2^21  ==> 12 hours for 2^30

// Okay, max vmalloc is 2^28,so going with that and doing hammer 4x w/ diff base addr
// 84s for 2^20 ==> 6 hours


//...That didn't work
// Trying to malloc 8 arrays of 2^28
// 81s for 2^18 ==> 23 hours for 2^28 well we don't have that luxury right now
// So lets do 8 arrays of 2^27 == 4 arrays of 2^28 ==> 12 hours

//That runs out of memory and never works. Going back to origin 2^28 and trying
//more than 4 base hammer addresses




#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/kthread.h>  // for threads
#include <linux/sched.h>  // for task_struct
#include <linux/time.h>   // for using jiffies 
#include <linux/jiffies.h>
#include <linux/timer.h>
#include <linux/tty.h>		/* For the tty declarations */
#include <linux/version.h>	/* For LINUX_VERSION_CODE */
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/smp.h>
#include <linux/vmalloc.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Marshall Versteeg");



extern unsigned long volatile jiffies;
static const unsigned long MEM_SIZE = 2 << 16;//(2 ^ 30); //1 GigaBYTE
static const unsigned int ROWBUFF_LOOP_COUNT = 1000000;//1MIL
// static const unsigned int ROWBUFF_LOOP_COUNT_TWO = 1000000;//1MIL
static const unsigned int HAMMER_LOOP_COUNT = 2000000;//2MIL
//TIME(minutes) = LOOP_COUNT * MEM_SIZE / 52428800000

static void print_string(char *);

static struct task_struct *thread1;
static struct task_struct *thread2;

typedef struct node {
    int * addr;
    struct node * next;
} node_t;

static int thread_fn_v2(void * void_pntr) {
	int i = 0;
	int ** params;
	int * addr1;
	int * addr2;
	int tmpres1;
	int tmpres2;
	params = (int **) void_pntr;

	// printk(KERN_INFO "addr1: %p  addr2: %p", addr1, addr2);
	addr1 = params[0];
	addr2 = params[1];
	
	for(i = 0; i < HAMMER_LOOP_COUNT; ++i){
		asm volatile(
			"ldr %[tmpres1], [%[addr1]];"
			// "dsb ish;"
			"ldr %[tmpres2], [%[addr2]];"
			// "dsb ish;"
			"mcr p15, 0, %[addr1], c7, c14, 1;"  // DCCIMVAC: Data cache clean and invalidate by MVA to PoC
			"mcr p15, 0, %[addr2], c7, c14, 1;"  //These two lines clflush addresses r4, r5
			// // "mcr p15, 0, r6, c7, c14, 1;" //Using these instead prove that cache lines are being
			// // "mcr p15, 0, r7, c7, c14, 1;" //flushed, b/c it runs a lot faster with these lines isntesad
			"dsb ish;"
			: [tmpres1]"=r" (tmpres1),
			[tmpres2]"=r" (tmpres2)
			: [addr1]"r" (addr1),
			[addr2]"r" (addr2)
		);		    
	}
	while(!kthread_should_stop());
	//kthread_should_stop, so we do that.
	do_exit(1);
	// return 0;
}

static int TwoGB_init(void) {
	unsigned long num_addresses = 0;
	unsigned long num_buff = 0;
	int ** params1;
	int ** params2;
	node_t * head;
	node_t * tail;
	node_t * ptr1;
	node_t * ptr2;
	int * rowhammer_mem;
	unsigned long i = 0;
	int const_val = 0xA55A5AA5;
	int err = 0;
	int pid;
	unsigned long prev_time, current_time;
	int * addr1;
	int * addr2;
	int tmpres1;
	int tmpres2;
	node_t * tmp;
	unsigned long lp = 0;
	int k = 0;
	int ret = 0;

	// printk(KERN_INFO "sizeof(int) == %d", sizeof(int)); //ITS 4

	prev_time = jiffies;
	pid = smp_processor_id();
	printk(KERN_INFO "master PID: %d", pid);
	printk(KERN_INFO "thread1 PID: %d", (pid + 1) % 4);
	params1 = (int **) vmalloc(sizeof(int *)*2);
	params2 = (int **) vmalloc(sizeof(int *)*2);

	/*=======================================
	=            Initialize MEM             =
	=======================================*/	
	rowhammer_mem = (int * ) vmalloc(MEM_SIZE);

	if(!rowhammer_mem){
		print_string("Failed to vmalloc. Dying...");
		return 1;
	}

	head = vmalloc(sizeof(node_t));
	head->addr = rowhammer_mem;
	head->next = NULL;
	tail = head;
	printk(KERN_INFO "MEM_SIZE: %ld", MEM_SIZE);
	i = 0;	
	for(; i < MEM_SIZE / sizeof(int); ++i){
		rowhammer_mem[i] = const_val;
	}
	printk(KERN_INFO "Rowhammer mem base virtual address: %p\n@@@\n", rowhammer_mem);
	print_string("Initialized mem, starting timing analysis");


	/*=======================================
	=   Start rowbuffer timing analysis     =
	=======================================*/	
	addr1 = rowhammer_mem;
	i = 32; //feels safer having some offset
	for(; i < MEM_SIZE / sizeof(int); i+=2048, ++num_addresses){
	    addr2 = (rowhammer_mem+i);
	    prev_time = jiffies;
		
		for(lp = 0; lp < ROWBUFF_LOOP_COUNT; ++lp){
			asm volatile(
				"ldr %[tmpres1], [%[addr1]];"
				// "dsb ish;"
				"ldr %[tmpres2], [%[addr2]];"
				// "dsb ish;"
				"mcr p15, 0, %[addr1], c7, c14, 1;"  // DCCIMVAC: Data cache clean and invalidate by MVA to PoC
				"mcr p15, 0, %[addr2], c7, c14, 1;"  //These two lines clflush addresses r4, r5
				// // "mcr p15, 0, r6, c7, c14, 1;" //Using these instead prove that cache lines are being
				// // "mcr p15, 0, r7, c7, c14, 1;" //flushed, b/c it runs a lot faster with these lines isntesad
				"dsb ish;"
				: [tmpres1]"=r" (tmpres1),
				[tmpres2]"=r" (tmpres2)
				: [addr1]"r" (addr1),
				[addr2]"r" (addr2)
			);		    
		}
		current_time = jiffies;
		
		//Create a linked list node of all the ones we want to hammer
		if(current_time-prev_time > 12){
			++num_buff;
			tmp = vmalloc(sizeof(node_t));
			tmp->addr = addr2;
			tmp->next = NULL;
			tail->next = tmp;
			tail = tail->next;
		}
		printk(KERN_INFO "%lu jiffies for addr1:%p addr2:%p", (current_time-prev_time), addr1, addr2);
	}
	printk("@@@\n@@@\n@@@");

	// addr1 = rowhammer_mem;
	// i = 32; //feels safer having some offset
	// for(; i < MEM_SIZE / sizeof(int); i+=64, ++num_addresses){
	//     addr2 = (rowhammer_mem+i);
	//     prev_time = jiffies;
		
	// 	for(lp = 0; lp < ROWBUFF_LOOP_COUNT_TWO; ++lp){
	// 		asm volatile(
	// 			"ldr %[tmpres1], [%[addr1]];"
	// 			// "dsb ish;"
	// 			"ldr %[tmpres2], [%[addr2]];"
	// 			// "dsb ish;"
	// 			"mcr p15, 0, %[addr1], c7, c14, 1;"  // DCCIMVAC: Data cache clean and invalidate by MVA to PoC
	// 			"mcr p15, 0, %[addr2], c7, c14, 1;"  //These two lines clflush addresses r4, r5
	// 			// // "mcr p15, 0, r6, c7, c14, 1;" //Using these instead prove that cache lines are being
	// 			// // "mcr p15, 0, r7, c7, c14, 1;" //flushed, b/c it runs a lot faster with these lines isntesad
	// 			"dsb ish;"
	// 			: [tmpres1]"=r" (tmpres1),
	// 			[tmpres2]"=r" (tmpres2)
	// 			: [addr1]"r" (addr1),
	// 			[addr2]"r" (addr2)
	// 		);		    
	// 	}
	// 	current_time = jiffies;
		
	// 	// //Create a linked list node of all the ones we want to hammer
	// 	// if(current_time-prev_time > 12){
	// 	// 	++num_buff;
	// 	// 	tmp = vmalloc(sizeof(node_t));
	// 	// 	tmp->addr = addr2;
	// 	// 	tmp->next = NULL;
	// 	// 	tail->next = tmp;
	// 	// 	tail = tail->next;
	// 	// }
	// 	printk(KERN_INFO "%lu jiffies for addr1:%p addr2:%p", (current_time-prev_time), addr1, addr2);
	// }

	print_string("Timing analysis complete, starting rowhammer");
	printk(KERN_INFO "Timing analysis complete, starting rowhammer");
	/*==========================================================
	=            Rowhammer just the nodes we added.            =
		For now, just do single-ended w/ garbage == rowhammer_mem
		Also try:
			- Increse MEM_SIZE
			- Try threaded double hammer O(n^2)
	==========================================================*/
	// ptr1 = head;
	// while(ptr1 != NULL){
	// 	addr2 = ptr1->addr;
	//     prev_time = jiffies;
		
	// 	for(lp = 0; lp < HAMMER_LOOP_COUNT; ++lp){
	// 		asm volatile(
	// 			"ldr %[tmpres1], [%[addr1]];"
	// 			"dsb ish;"
	// 			"ldr %[tmpres2], [%[addr2]];"
	// 			"dsb ish;"
	// 			"mcr p15, 0, %[addr1], c7, c14, 1;"  // DCCIMVAC: Data cache clean and invalidate by MVA to PoC
	// 			"mcr p15, 0, %[addr2], c7, c14, 1;"  //These two lines clflush addresses r4, r5
	// 			// // "mcr p15, 0, r6, c7, c14, 1;" //Using these instead prove that cache lines are being
	// 			// // "mcr p15, 0, r7, c7, c14, 1;" //flushed, b/c it runs a lot faster with these lines isntesad
	// 			"dsb ish;"
	// 			: [tmpres1]"=r" (tmpres1),
	// 			[tmpres2]"=r" (tmpres2)
	// 			: [addr1]"r" (addr1),
	// 			[addr2]"r" (addr2)
	// 		);		    
	// 	}
	// 	current_time = jiffies;
	// 	printk(KERN_INFO "%lu jiffies for addr1:%p addr2:%p", (current_time-prev_time), addr1, addr2);
	// 	ptr1 = ptr1->next;
	// }

	/*===============================================================
	=            Do double-ended, multi-threaded hammmer            =
	===============================================================*/
	// If one thread's garbage address hits in the row buffer, that's good b/c less time spent not hammering
	// ptr1 = head;
	// ptr2 = head;
    
    params1[0] = head->addr;
    params1[1] = tail->addr;
    params1[1] = tail->addr;

	// for(k = 0; k < 6; ++k){
		ptr2 = head;
		while(ptr2 != NULL){

			params1[0] = ptr2->addr;
			thread1 = kthread_create(thread_fn_v2, (void *) params1, "thread1");
			thread2 = kthread_create(thread_fn_v2, (void *) params1, "thread2");
			kthread_bind(thread1, ((pid + 1) % 4));
			kthread_bind(thread1, ((pid + 2) % 4));
		    
		    prev_time = jiffies;
		    if(thread1){
		        wake_up_process(thread1);
		    }
		    if(thread2){
		        wake_up_process(thread2);
		    }
			
			msleep(1);
			ret = kthread_stop(thread1);
			ret = kthread_stop(thread2);
			current_time = jiffies;
			
			printk(KERN_INFO "%lu jiffies for addr1:%p addr2:%p", (current_time-prev_time), addr1, addr2);
			ptr2 = ptr2->next;
		}
	// ptr1 = ptr1->next->next->next;
	// addr1 = ptr1->addr;
	// }
	
	/*===========================================
	=            Check for bit flips            =
	===========================================*/
    print_string("Finished hammering, checking for errors...");
	i = 0;	
	for(; i < MEM_SIZE / sizeof(int); ++i){
		if(rowhammer_mem[i] != const_val){
			++err;
			printk(KERN_ALERT "Error at mem[%ld]: %d", i, rowhammer_mem[i]);
		}
	/*==========================================
	=            Delete linked list            =
	==========================================*/
	ptr1 = head;
	while(ptr1 != NULL){
		ptr2 = ptr1->next;
		vfree(ptr1);
		ptr1 = ptr2;
	}
	
	}
	if(err == 0)
		print_string("No Errors :(");
	else
		print_string("Errors!");
	printk(KERN_INFO "Total # errors: %d", err);

    vfree(rowhammer_mem);
    printk(KERN_INFO "Total hits:%ld / Total addrs:%ld", num_buff,num_addresses);
	return 0;

}



static void TwoGB_exit(void) {
	printk(KERN_INFO "thread_test_exit\n\n");
}

module_init(TwoGB_init);
module_exit(TwoGB_exit);


static void print_string(char *str)
{
	struct tty_struct *my_tty;
	#if ( LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,5) )
		my_tty = current->tty;
	#else
		my_tty = current->signal->tty;
	#endif

		if (my_tty != NULL) {
			((my_tty->ops)->write) (my_tty,	/* The tty itself */
	#if ( LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,9) )		
						   0,	/* Don't take the string 
							   from user space        */
	#endif
						   str,	/* String                 */
						   strlen(str));	/* Length */
	#if ( LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,9) )		
			((my_tty->ops)->write) (my_tty, 0, "\015\012", 2);
	#else
			((my_tty->ops)->write) (my_tty, "\015\012", 2);
	#endif
	}
}

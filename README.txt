Contributors: Marshall Versteeg, Bradley England

This repository contains all of our collected data (dispered in .csv files in the base directory and  ./kern/my_kernel), some of the initial testing files (test_arm_asm.cpp) and several iterations of our rowhammer attack (thread_test.c then thread_test_v2.c then TwoGB_test.c) which are in ./kern/my_kernel.

To EECS573 course staff:
The rowhammer test relies on the raspberry pi linux source and ARM assembly, so its not going to work on any other device. You can ssh into the rpi if you'd like, just email bradengl@umich.edu for the ip and password.

All of the rowhammer tests in ./kern/my_kernel are ran as loadable kernel modules, targeting the rpi_src linux kernel. 

To run the test:
	$ cd kern/my_kernel
	$ make
	$ sudo rmmod TwoGB_test #remove module in case it was already loaded
	$ cp one.txt /sys/module/rcupdate/parameters/rcu_cpu_stall_suppress #suppress stall warnings in dmesg
	$ sudo dmesg -c 	# Clear dmesg, b/c this test outputs a ton of crap
	$ time sudo insmod TwoGB_test.ko #run rowhammer test and record time

To vew the results:
	$ dmesg

Note: The Makefile in kern/my_kernel targets the raspberry pi's linux source code, so it won't work if you port it to another target machine. Also, the rowhammer tests have inlined arm asm so they obviously won't work on non-ARM devices.


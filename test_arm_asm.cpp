#include <cstdlib>
#include <cstdio>



int main(){
	unsigned int y, x;
	x = 16;

	//Assembly rotate right 1 example
	asm("mov    %[result], %[value], ror #1"

	           : [result]"=r" (y) /* Rotation result. */
	           : [value]"r"   (x) /* Rotated value. */
	           : /* No clobbers */
	    );

	printf("Value: %x	Result: %x\n", x, y);


	// Assembly read SCTLR, program crashes if this is run
	unsigned int id = 0;
	// asm volatile ("MRC    p15, 0, %[result], c0, c0, 0"
	//            : [result]"=r" (id)  //Read Main ID Result.
	//     );

	int c1, c2, read_count;
	read_count = 50;
	int * f = (int *) malloc(sizeof(int));
	*f = 13;
	int * s = (int *) malloc(sizeof(int));
	*s = 12;

 asm volatile
    (
    "mov r3, %2;"                   // r3 = #read_count
    "mov r4, %3;"                   // r4 = f
    "mov r5, %4;"                   // r5 = s
    "mrc p15, 0, %0, c9, c13, 0;"   // t1 = rdtsc() -- I would remove this
    "dsb ish;"
"ds_loop:;"
    "ldr r2, [r4];"
    "ldr r2, [r5];"
    "mcr p15, 0, r4, c7, c14, 1;"  // DCCIMVAC: Data cache clean and invalidate by MVA to PoC
    "mcr p15, 0, r5, c7, c14, 1;"
    "subs r3, r3, #1;"
    "bcs ds_loop;"
    "dsb ish;"
    "mrc p15, 0, %1, c9, c13, 0;"   // t2 = rdtsc() -- Remove this too
            : "=r" (c1),
              "=r" (c2)
            : "r" (/*hammer_addresses->*/read_count),
              "r" (/*hammer_addresses->*/f),
              "r" (/*hammer_addresses->*/s)
            : "r2", "r3", "r4", "r5", "memory"
    );

	printf("Got here\n");

	// printf("SCTLR: %x", id);


	return 0;
}